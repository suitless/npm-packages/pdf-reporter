import { Component } from '@angular/core';
import { PdfReporterService } from 'pdf-reporter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'pdf-reporter-demo';

  privateAnswersJson = `{
    "content": [
      {
        "text": "The Legal Healthcheck Platform is not your online lawyer. Therefore, the service itself and the information it provides shall not be seen as legal advice. Rather, the Legal Healthcheck platform is intended as educational tool for students and start-ups to obtain basic legal knowledge. Suitless cannot guarantee that the information provided on this website (including the reports that it generates) is correct and fully up-to-date. Usage of the Legal Healthcheck Platform is fully at your own risk. Suitless explicitly rejects all liability for direct, indirect, incidental or special damages arising out or relating to the access or use of the service. This includes, but not restricted to, loss or damage caused by usage of information by this website, inaccurate results, loss of profits, business interruption, loss of use of the service, the cost of substitute services or claims by third parties for any damages to computers, software, modems, telephones or other property.",
        "type": 0
      },
      {
        "text": "How are you today?",
        "type": 1
      },
      {
        "text": "I am fine.",
        "type": 2
      },
      {
        "text": "You are feeling fine.",
        "type": 3
      },
      {
        "text": "You have indicated that you are feeling fine.",
        "type": 4
      },
      {
        "text": "How were you yesterday?",
        "type": 1
      },
      {
        "text": "I was fine.",
        "type": 2
      },
      {
        "text": "You were feeling fine.",
        "type": 3
      },
      {
        "text": "This is a second implication. And also a very long text which is a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a.",
        "type": 3
      },
      {
        "text": "This is a third implication. And also a very long text which is a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a.",
        "type": 3
      },
      {
        "text": "This is a fourth implication. And also a very long text which is a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a.",
        "type": 3
      },
      {
        "text": "This is a sixth implication. And also a very long text which is a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a a.",
        "type": 3
      },
      {
        "text": "Depending on the license of the open source software you used, it is possible (and likely) that the parts of the software you created that is not seprable from the open source software cannot be protected by copyright. Please find the license in the software packkage you used. For a general overview this wikipedia table could be helpfull: https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses.",
        "type": 3
      },
      {
        "text": "How are you going to be tomorrow? das adas das dasd asd asd adas dasd asd asd asd asdasda asdas d",
        "type": 1
      },
      {
        "text": "I am going to be fine.",
        "type": 2
      },
      {
        "text": "You indicated that you are going to feel fine.",
        "type": 3
      },
      {
        "text": "Will this create a new page?",
        "type": 1
      },
      {
        "text": "I hope so.",
        "type": 2
      }
    ],
    "indexTitle": "Cool module",
    "moduleName": "IP & Placeholders",
    "user": "Martijn de Vlam",
    "business": "Suitless & Co."
}`;

  constructor(private pdf: PdfReporterService) {}

  generatePDF() {
    let content = JSON.parse(this.privateAnswersJson);
    console.log(content);
    this.pdf.generatePdf('http://localhost:4200/assets/template.pdf', content);
  }
}
