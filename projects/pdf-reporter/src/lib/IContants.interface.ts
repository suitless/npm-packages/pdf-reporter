export interface IConstants {
  fontSize: number;
  padding: number;
  implicationMaxWidth: number;
  speechMaxWidth: number;
}

export let constants: IConstants = {
  fontSize: 12,
  padding: 10,
  speechMaxWidth: 310,
  implicationMaxWidth: 420,
};
