import { PDFPage, RGB, PDFFont } from 'pdf-lib';

export abstract class SvgDrawable {
  /**
   * SVG Path for the speech bubble.
   */
  public speechBubble: string;
  /**
   * SVG Path for the implication bubble.
   */
  public implicationBubble: string;

  /**
   * Adds a speech bubble to a page
   * @param page The PDFPage
   * @param colour The colour of the speech bubble
   * @param shapes The shapeSet
   * @param font The font of the text
   * @param text The actual text
   */
  public drawSpeech(page: PDFPage, colour: RGB, font: PDFFont, text: string): number {
    return null;
  }

  /**
   * Adds an implication bubble to a page
   * @param page The PDFPage
   * @param colour The colour of the speech bubble
   * @param shapes The shapeSet
   * @param font The font of the text
   * @param text The actual text
   */
  public drawImplication(page: PDFPage, colour: RGB, font: PDFFont, text: string): number {
    return null;
  }
}
