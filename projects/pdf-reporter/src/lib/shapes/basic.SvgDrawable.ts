import { SvgDrawable } from './SvgDrawable.class';
import { PDFPage, RGB, PDFFont, rgb } from 'pdf-lib';
import * as svgpath from 'svgpath';
import { constants } from '../IContants.interface';

export const basicSvgShapes = new (class BasicSvgShapes implements SvgDrawable {
  speechBubble = `m 0 0 l 0 5.5 l 110 0 l 0 -5.5 l -110 0`;
  implicationBubble = `m 0 0 l 0 5.5 l 150 0 l 0 -5.5 l -150 0`;

  public drawSpeech(page: PDFPage, colour: RGB, font: PDFFont, text: string) {
    return this.draw(page, colour, font, text, constants.speechMaxWidth, this.speechBubble);
  }

  public drawImplication(page: PDFPage, colour: RGB, font: PDFFont, text: string) {
    return this.draw(page, colour, font, text, constants.implicationMaxWidth, this.implicationBubble);
  }

  public drawDisclaimer(page: PDFPage, colour: RGB, font: PDFFont, text: string) {
    const svg = new svgpath(this.implicationBubble).scale(1, 2).toString();
    page.drawSvgPath(svg, {
      color: colour,
      scale: 3,
    });
    page.drawText('Disclaimer', {
      x: page.getX() + 15,
      y: page.getY() - 24 - 1,
      maxWidth: 420,
      lineHeight: 15,
      size: 24,
      font,
      color: rgb(0, 0, 0),
    });
    page.moveTo(80, page.getY() - font.heightAtSize(24) - 15);
    return (
      this.draw(page, colour, font, text, constants.implicationMaxWidth, this.implicationBubble) + font.heightAtSize(24)
    );
  }

  public draw(page: PDFPage, colour: RGB, font: PDFFont, text: string, maxWidth: number, svgPath: string) {
    const lines = this.getTextLines(font, constants.fontSize, maxWidth, text);
    const svg = new svgpath(svgPath).scale(1, lines).toString();

    page.drawSvgPath(svg, {
      color: colour,
      scale: 3,
    });
    page.drawText(text, {
      x: page.getX() + 15,
      y: page.getY() - constants.fontSize - 1,
      maxWidth,
      lineHeight: 15,
      size: constants.fontSize,
      font,
      color: rgb(0, 0, 0),
    });
    return this.getHeightFromLines(font, lines);
  }

  public getHeightFromLines(font: PDFFont, lines: number) {
    return constants.padding + lines * (font.heightAtSize(constants.fontSize) + 5);
  }

  public getHeight(font: PDFFont, maxWidth: number, text: string) {
    return (
      constants.padding +
      this.getTextLines(font, constants.fontSize, maxWidth, text) * (font.heightAtSize(constants.fontSize) + 5)
    );
  }

  public checkWordLengths(text: string, maxWidth: number, font: PDFFont) {
    let newText = '';
    const words = text.split(' ');
    words.forEach((word) => {
      const width = font.widthOfTextAtSize(word.replace(/\r?\n|\r/g, ''), constants.fontSize);
      if (width > maxWidth) {
        const maxWordLength = (maxWidth * word.length) / width;
        newText += word.substring(0, maxWordLength) + '\n' + word.substring(maxWordLength + 1, word.length) + ' ';
      } else {
        newText += word + ' ';
      }
    });
    return newText;
  }

  //TODO: fix, sometimes it counts one line to little (maybe padding issue?)
  public getTextLines(font: PDFFont, size: number, maxWidth: number, text: string) {
    const textPieces = text.split(/\r?\n|\r/g);
    let textLines = 0;
    textPieces.forEach((tp) => {
      textLines += Math.ceil(font.widthOfTextAtSize(tp, size) / maxWidth);
    });
    return textLines;
  }
})();
