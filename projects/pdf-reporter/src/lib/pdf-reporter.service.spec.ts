import { TestBed } from '@angular/core/testing';

import { PdfReporterService } from './pdf-reporter.service';

describe('PdfReporterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PdfReporterService = TestBed.get(PdfReporterService);
    expect(service).toBeTruthy();
  });
});
