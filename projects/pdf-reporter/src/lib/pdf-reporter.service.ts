import { Injectable } from '@angular/core';
import { IReport, IReportContent, EReportContentType } from '@suitless/module-domain';
import { PDFDocument, rgb, StandardFonts, PDFPage, PDFFont } from 'pdf-lib';
import download from 'downloadjs';
import { basicSvgShapes as svgShapes } from './shapes/basic.SvgDrawable';
import { constants } from './IContants.interface';

@Injectable({
  providedIn: 'root',
})
export class PdfReporterService {
  constructor() {}

  /**
   * Generates a PDF report and offers the user a download
   * @param templateUrl Template base URL
   * @param report Report content from the flowchart-walker
   */
  public async generatePdf(templateUrl: string, report: IReport): Promise<void> {
    const templatyePdfBytes = await fetch(templateUrl).then((res) => res.arrayBuffer());
    const pdfDoc = await PDFDocument.load(templatyePdfBytes);
    const fontCourier = await pdfDoc.embedFont(StandardFonts.Courier);
    const fontHelvetica = await pdfDoc.embedFont(StandardFonts.Helvetica);

    // Set PDF MetaData
    pdfDoc.setTitle('Suitless Report');
    pdfDoc.setAuthor('Suitless'); //TODO: Add module owner
    pdfDoc.setSubject('');
    pdfDoc.setKeywords(['Suitless', 'Report']);
    pdfDoc.setProducer('Suitless');
    pdfDoc.setCreator('Suitless PDF-Reporter');
    pdfDoc.setCreationDate(new Date());

    // Add date, module, Business and user. Offset = 22
    const firstPage = pdfDoc.getPages()[0];
    this.addText(firstPage, new Date().toDateString(), 160, 660, 16, fontCourier);
    this.addText(firstPage, report.moduleName, 160, 638, 16, fontCourier);
    this.addText(firstPage, report.business, 160, 614, 16, fontCourier);
    this.addText(firstPage, report.user, 160, 591, 16, fontCourier);

    // Add Content
    const { width, height } = firstPage.getSize();

    let yOffset = 0;
    for (const i of Object.keys(report.content)) {
      // The content should fit, and leave 50 height spare.
      let yClearing = 50 + svgShapes.getHeight(fontHelvetica, constants.speechMaxWidth, report.content[i].text);
      if (report.content[i].type === EReportContentType.Question && report.content[Number(i) + 1]) {
        // Questions should always have room for their matching answer under them.
        yClearing += svgShapes.getHeight(fontHelvetica, constants.speechMaxWidth, report.content[Number(i) + 1].text);
      }

      if (yOffset < yClearing) {
        pdfDoc.addPage([width, height]);
        yOffset = pdfDoc.getPages()[pdfDoc.getPageCount() - 1].getHeight() - 50;
      }
      yOffset -= this.addReportContent(pdfDoc, fontHelvetica, yOffset, report.content[i]);
    }

    // Offer the user a download
    const pdfBytes = await pdfDoc.save();
    download(pdfBytes, 'Suitless-report.pdf', 'application/pdf');
  }

  /**
   * Adds reportContent to the last page of a document.
   * @param document The PDFContent
   * @param font The font
   * @param y Y Coord
   * @param content The content model
   */
  private addReportContent(document: PDFDocument, font: PDFFont, y: number, content: IReportContent): number {
    const lastPage = document.getPages()[document.getPageCount() - 1];
    switch (content.type) {
      case EReportContentType.Answer:
        lastPage.moveTo(225, y);
        return svgShapes.drawSpeech(
          lastPage,
          rgb(0.9, 0.9, 1),
          font,
          svgShapes.checkWordLengths(content.text, constants.speechMaxWidth, font)
        );

      case EReportContentType.Question:
        lastPage.moveTo(30, y);
        return svgShapes.drawSpeech(
          lastPage,
          rgb(1, 0.9, 0.9),
          font,
          svgShapes.checkWordLengths(content.text, constants.speechMaxWidth, font)
        );

      case EReportContentType.Implication:
        lastPage.moveTo(80, y);
        return svgShapes.drawImplication(
          lastPage,
          rgb(1, 0.8, 0.4),
          font,
          svgShapes.checkWordLengths(content.text, constants.implicationMaxWidth, font)
        );

      case EReportContentType.Notification:
        lastPage.moveTo(80, y);
        return svgShapes.drawImplication(
          lastPage,
          rgb(0.9, 0.9, 0.9),
          font,
          svgShapes.checkWordLengths(content.text, constants.implicationMaxWidth, font)
        );

      case EReportContentType.Disclaimer:
        lastPage.moveTo(80, y);
        return (
          svgShapes.drawDisclaimer(
            lastPage,
            rgb(0.9, 0.9, 1),
            font,
            svgShapes.checkWordLengths(content.text, constants.implicationMaxWidth, font)
          ) + lastPage.getSize().height
        );
    }
  }

  /**
   * Adds simple text to a page
   * @param pdf The PDFPage
   * @param text The text
   * @param x X Coord
   * @param y Y Coord
   * @param size Font Size
   * @param font Font
   */
  private addText(pdf: PDFPage, text: string, x: number, y: number, size: number, font: PDFFont) {
    pdf.drawText(text, {
      x,
      y,
      size,
      font,
      color: rgb(0, 0, 0),
    });
  }
}
